
function createShip(){
     //use the data from the form to create new ship
     shipName = document.getElementById("shipName").value
     maxSped = document.getElementById("maxSped").value 
     range= document.getElementById("range").value 
     costPerKm = document.getElementById("costPerKm").value
     descr=document.getElementById("descr").value
     
     status = 'available'
     
    var new_ship = new Ship(shipName,maxSped,range,costPerKm,descr,status);
    var ship_list = Retrieve();
    ship_list._ship.push(new_ship);
    Serialization(ship_list);
}
function Retrieve(){
    //Extract the shipList data from localStorage
    var shipString = localStorage.getItem("shipList") || "";
    var ship_list = new ShipList();
    if(shipString!="")
        ship_list._ship = JSON.parse(shipString);
    else 
        ship_list._ship=[];
    return ship_list;
}
function Serialization(ship_list){
    //save the new ship in localStorage
    localStorage.setItem("shipList",JSON.stringify(ship_list._ship));
    alert("Create Ship Successfully");
}
