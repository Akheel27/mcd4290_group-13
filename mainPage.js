function RouteDetail(routeName){
    //when open viewRoute.html,use the routeName to find the selected route to display all related information
    localStorage.setItem("DetailRouteName",routeName);
    window.open("viewRoute.html");
}


var Route = new routeList();
//Extract the routeList data from localStorage
Route._route = JSON.parse(localStorage.getItem("routeList"));
var tbodyElement = document.getElementsByTagName("tbody")[0];
var routeElement = document.getElementsByClassName("routeData")[0];
if(Route._route==null || Route._route.length==0){
    alert("no routes");
}
//Add a click event to each row of data to redirect related pag
for (let route of Route._route) {
    var new_route = routeElement.cloneNode(true);
    new_route.children[0].innerText = route._routeName;
    new_route.children[1].innerText = route._dist;
    new_route.children[2].innerText = route._cost;
    new_route.children[3].innerText = route._time;
    new_route.onclick = ()=>{
        RouteDetail(route._routeName);
    }
    tbodyElement.appendChild(new_route);
}
tbodyElement.removeChild(routeElement);