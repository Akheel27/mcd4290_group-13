var Ship_list = new ShipList();
Ship_list._ship = JSON.parse(localStorage.getItem("shipList"));
var tbodyElement = document.getElementsByTagName("tbody")[0];
var shipElement = document.getElementsByClassName("shipData")[0];
if(Ship_list._ship==null){
    alert("no Ships");
}
for (let ship of Ship_list._ship) {
    var new_ship = shipElement.cloneNode(true);
    new_ship.children[0].innerText = ship._shipName;
    new_ship.children[1].innerText = ship._maxSped;
    new_ship.children[2].innerText = ship._range;
    new_ship.children[3].innerText = ship._costPerKm;
    new_ship.children[4].innerText = ship._descr;
    new_ship.children[5].innerText = ship._status;
    tbodyElement.appendChild(new_ship);
}
tbodyElement.removeChild(shipElement);