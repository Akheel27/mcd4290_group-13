var Port_list = new PortList();
Port_list._port = JSON.parse(localStorage.getItem("PortList"));
var tbodyElement = document.getElementsByTagName("tbody")[0];
var portElement = document.getElementsByClassName("portData")[0];
if(Port_list._port==null){
    alert("no ports");
}
for (let port of Port_list._port) {
    var new_port = portElement.cloneNode(true);
    new_port.children[0].innerText = port.portName;
    new_port.children[1].innerText = port.portCountry;
    new_port.children[2].innerText = port.portLocation;
    new_port.children[3].innerText = port.portSize;
    new_port.children[4].innerText = port.portType;
    new_port.children[5].innerText = port.portLatitude;
    new_port.children[6].innerText = port.portLongitude;
    tbodyElement.appendChild(new_port);
}
tbodyElement.removeChild(portElement);