mapboxgl.accessToken = 'pk.eyJ1IjoiYXNodWxvdmUiLCJhIjoiY2tlcDJ1NzduMWg1aDJ5bXNndTl3ZDZ0diJ9.d9pCj-IB1SW-2oQsxPbDyg';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
    center: [-122.48369693756104, 37.83381888486939], // starting position [lng, lat]
    zoom: 9 // starting zoom
});

map.on('mousemove', function (e) {
    document.getElementById('info').innerHTML =
        // e.lngLat is the longitude, latitude geographical position of the event
        JSON.stringify(e.lngLat.wrap());
});

map.on('click', function (e) {

    // e.lngLat is the longitude, latitude geographical position of the event
    var newMarker = new mapboxgl.Marker()
        .setLngLat([e.lngLat.lng, e.lngLat.lat])
        .addTo(map)
    linedata.data.geometry.coordinates.push([e.lngLat.lat,e.lngLat.lng]);
    creatingLine(linedata)

});

var linedata= {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': [
                    
                ]
            }
        }
    }



var startPortIndex = 0;
var endPortIndex = 0;
var shipIndex = 0;

var startPortsContainer = document.getElementById("startPortList");
var endPortsContainer = document.getElementById("endPortList");
var shipContainer = document.getElementById("shipList");
var port_list = JSON.parse(localStorage.getItem("portList")) || [];
var ship_list = JSON.parse(localStorage.getItem("shipList")).filter(ship=>ship._status!="en-route") || [];

GetdataFromAPI("https://eng1003.monash/api/v1/ports/", "GET", port_list, "ports");
GetdataFromAPI("https://eng1003.monash/api/v1/ships/", "GET", ship_list, "ships");


function initView(port_list, ship_list) {
    for (let index = 0; index < port_list ?.length; index++) {
        var new_port = document.createElement("option");
        new_port.value = port_list[index].portName || port_list[index].name;
        
        
        startPortsContainer.appendChild(new_port);

    }
   
    for (let index = 0; index < port_list ?.length; index++) {
        var new_port = document.createElement("option");
        new_port.value = port_list[index].portName || port_list[index].name;
        
        endPortsContainer.appendChild(new_port);
    }

    for (let index = 0; index < ship_list ?.length; index++) {
        var new_ship = document.createElement("option");
        new_ship.value = ship_list[index]._shipName || ship_list[index].name;
        
        shipContainer.appendChild(new_ship);
    }
}


   
         



var DistbetweenPort = 0
function createPort() {
    startPortIndex = port_list.findIndex(port=>port.name==document.getElementById("startPort").value || port.portName==document.getElementById("startPort").value);
    endPortIndex = port_list.findIndex(port=>port.name==document.getElementById("endPort").value || port.portName==document.getElementById("endPort").value);
    shipIndex = ship_list.findIndex(ship=>ship.name == document.getElementById("ship").value || ship._shipName== document.getElementById("ship").value);

    if (startPortIndex == endPortIndex) {
        alert("wrong!!!the startPort is same as the endPort");
        return;
    }
    if (document.getElementById("routeName").value == "") {
        alert("wrong!!!the routeName should not be empty");
        return;
    }
    //mark startPort
    new mapboxgl.Marker({
        color: '#00ff00'
    }).setLngLat([port_list[startPortIndex].portLongitude || port_list[startPortIndex].lng, port_list[startPortIndex].portLatitude || port_list[startPortIndex].lat]).addTo(map);
    //set map center
    map.setCenter([port_list[startPortIndex].portLongitude || port_list[startPortIndex].lng, port_list[startPortIndex].portLatitude || port_list[startPortIndex].lat]);
    //mark endPort
    new mapboxgl.Marker({
        color: '#ff0000'
    }).setLngLat([port_list[endPortIndex].portLongitude || port_list[endPortIndex].lng, port_list[endPortIndex].portLatitude || port_list[endPortIndex].lat]).addTo(map);
    map.setZoom(0);
    
    linedata.data.geometry.coordinates.push(
    [port_list[startPortIndex].portLongitude || port_list[startPortIndex].lng,port_list[startPortIndex].portLatitude || port_list[startPortIndex].lat],
    [port_list[endPortIndex].portLongitude || port_list[endPortIndex].lng, port_list[endPortIndex].portLatitude || port_list[endPortIndex].lat]
    )

    creatingLine(linedata)
 
    var DistbetweenPort = distanceBetweenPoints(port_list[startPortIndex].portLatitude || port_list[startPortIndex].lat, port_list[startPortIndex].portLongitude || port_list[startPortIndex].lng, port_list[endPortIndex].portLatitude || port_list[endPortIndex].lat, port_list[endPortIndex].portLongitude || port_list[endPortIndex].lng)
    document.getElementById('distance').innerHTML ='distance = ' + DistbetweenPort
}

function creatingLine (linedata) {
     
         map.on('load', function () {
         map.addSource('route', linedata).addTo(map);   
         map.addLayer({
                    'id': 'route',
                    'type': 'line',
                    'source': 'route',
                    'layout': {
                    'line-join': 'round',
'                   line-cap': 'round'
                    },
                        'paint': {
                'line-color': '#f00',
                'line-width': 8
                    }
                });
         })
         }     

    
function Retrieve() {
    var routeString = localStorage.getItem("routeList") || "";
    var route_list = new routeList();
    if (routeString != "")
        route_list._route = JSON.parse(routeString);
    else
        route_list._route = [];
    return route_list;

}



function Serialization(route_list) {
    localStorage.setItem("routeList", JSON.stringify(route_list._route));
    alert("Create Route Successfully");

}

function GetdataFromAPI(url, method, data, porperty) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.onreadystatechange = () => {
        // get data from API
        if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 304) {
            // format data
            // data.concat(JSON.parse(xhr.responseText)[porperty]);
            data = data.concat(JSON.parse(xhr.responseText)[porperty]);
            if (porperty == "ports") {
                port_list = data;
                initView(data, undefined)
            }
            if (porperty == "ships") {
                ship_list = data;
                initView(undefined, data);
            }

        }
    };
    xhr.send();

}

function saveRoute(){
    var new_route = new route();
    new_route._routeName = document.getElementById("routeName").value;
    new_route._shipName = ship_list[shipIndex]._shipName || ship_list[shipIndex].name;
    new_route._sourcePort = port_list[startPortIndex];
    new_route._destinationPort = port_list[endPortIndex];
    new_route._startDate = document.getElementById("dateSelect").value;
    new_route._wayPointList = wayPointList

    var route_list = Retrieve();
    route_list._route.push(new_route);
    Serialization(route_list);

}


function distanceBetweenPoints(lat1, lng1, lat2, lng2) {
    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI / 180; // φ, λ in radians
    const φ2 = lat2 * Math.PI / 180;
    const Δφ = (lat2 - lat1) * Math.PI / 180;
    const Δλ = (lng2 - lng1) * Math.PI / 180;

    const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
        Math.cos(φ1) * Math.cos(φ2) *
        Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const d = R * c; // in metres
}