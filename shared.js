//Akheel
class Route {
    constructor (routeName, shipName,sourcePort, destinationPort, dist, time, cost, startDate, wayPointList){
        // private contributes
        this._routeName= routeName
        this._shipName = shipName
        this._destinationPort = destinationPort
        this._dist = dist
        this._time = time
        this._cost = cost
        this._startDate = startDate
        this._wayPointList = wayPointList                
    }
}

// done by PAN
class Port {
    constructor(portName,portCountry,portType,portSize,portLocation, portLongitude, portLatitude){
        this.portName = portName;
        this.portCountry = portCountry;
        this.portType = portType;
        this.portSize = portSize;
        this.portLocation = portLocation
        this.portLongitude = portLongitude
        this.portLatitude = portLatitude
    }
    

}


// matthew  code
class Ship {
    constructor(shipName,maxSped,range,costPerKm,descr,status) {
        this._shipName=shipName
        this._maxSped=maxSped
        this._range= range
        this._costPerKm= costPerKm
        this._descr=descr
        this._status= status
        
    }
}
    


//akheel
class RouteList {
    constructor(Route){
        this._route = [] // empty array
    }
}

class ShipList{
    constructor(new_ship){
        this._ship = []
    }
}

class PortList {
    constructor (newPort) {
        this._port = []
    }
}
    
